#!/bin/sh

# WARNING: This script supposed to be run in container environment

filename=commento.$(date -u +%Y-%m-%dT%H:%M:%S)Z.sql.gz

pg_dump -h db -U postgres commento 2>&1 | gzip > "/app/$filename"

# need to deploy ssh key in storage box first, see https://docs.hetzner.com/robot/storage-box/backup-space-ssh-keys/

scp -i /app/backup_ssh_key /app/$filename ${COMMENTO_BACKUP_USERNAME}@${COMMENTO_BACKUP_SERVER}:/
