package main

func printp(str string, vars ...string) string {
	isInBraces    := false
	indexInBraces := -1
        output        := ""

        for _,c := range str {
		switch c {
			case '{':
				if !isInBraces {
					isInBraces = true
				} else {
					output += string(c)
				}
			case '}':
				if isInBraces && indexInBraces != -1 {
					isInBraces = false
					output += string(vars[indexInBraces])
					indexInBraces = -1
				} else {
					output += string(c)
				}
			default:
				if isInBraces && c >= 48 && c <= 57 {
					indexInBraces = int(c) - 48
				}
		}
	}

	return output
}
