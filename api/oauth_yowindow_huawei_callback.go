package main

import (
	"os"
	"math/big"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"github.com/dgrijalva/jwt-go"
)

func yowindowHuaweiCallbackHandler(w http.ResponseWriter, r *http.Request) {
	commenterToken    := r.FormValue("commenterToken")
	idToken := r.FormValue("idToken")

	// check commenter token
	_, err := commenterGetByCommenterToken(commenterToken)

	if err != nil && err != errorNoSuchToken {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	// get user info from id token
	type Claims struct {
	    Email   string `json:"email"`
	    Picture string `json:"picture"`
	    Name    string `json:"name"`
	    DisplayName string `json:"display_name"`
	    FamilyName  string `json:"family_name"`
	    GivenName   string `json:"given_name"`
	    jwt.StandardClaims
	}

	claims := &Claims{}

	_, err = jwt.ParseWithClaims(idToken, claims, func(token *jwt.Token) (interface{}, error) {
		// Obtain a public key URI from the jwks_uri field in the response
		// The public key is updated once a day. Its value can be cached in your app server.
		resp, err := http.Get("https://oauth-login.cloud.huawei.com/.well-known/openid-configuration")
		contents, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if err != nil {
			fmt.Fprintf(w, "Error: %s", errorCannotReadResponse.Error())
			return nil, err
		}

		openidConfiguration := make(map[string]interface{})

		if err := json.Unmarshal(contents, &openidConfiguration); err != nil {
			fmt.Fprintf(w, "Error: %s", errorInternal.Error())
			return nil, err
		}

		publicKeyUri := openidConfiguration["jwks_uri"].(string)

		resp, err = http.Get(publicKeyUri)
		contents, err = ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if err != nil {
			fmt.Fprintf(w, "Error: %s", errorCannotReadResponse.Error())
			return nil, err
		}

		type keysConfiguration struct {
			Keys []map[string]string
		}

		k := &keysConfiguration{}

		if err := json.Unmarshal(contents, k); err != nil {
			fmt.Fprintf(w, "Error: %s", errorInternal.Error())
			return nil, err
		}

		var key rsa.PublicKey

		for _, object := range k.Keys {
			if object["kid"] == token.Header["kid"] {
				n, err := base64.RawURLEncoding.DecodeString( object["n"] )
				if err != nil {
					return nil, err
				}
				key.N = big.NewInt(0).SetBytes(n)

				e, err := base64.RawURLEncoding.DecodeString( object["e"] )
				if err != nil {
					return nil, err
				}
				key.E = int(big.NewInt(0).SetBytes(e).Uint64())
			}
		}

		return &key, nil
	})

	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	// 3. Check whether the value of iss in the ID token is https://accounts.huawei.com.
	if claims.Issuer != "https://accounts.huawei.com" {
		bodyMarshal(w, response{"success": false, "message": "token has wrong issuer"})
		return
	}

	// 4. Verify that the value of the aud field in the ID token is the same as the value of the client_id field of the app (the value of client_id is the app ID in the AppGallery Connect).
	if claims.Audience != os.Getenv("HUAWEI_CLIENT_ID") && claims.Audience != os.Getenv("HUAWEI_CLIENT_ID_DEMO") {
		bodyMarshal(w, response{"success": false, "message": "token has wrong audience"})
		return
	}

	// done with checking

	email := claims.Email

	if claims.Email == "" {
		email = claims.Subject + "@" + os.Getenv("HUAWEI_FAKE_EMAIL_DOMAIN")
	}

	c, err := commenterGetByEmail("huawei", email)

	if err != nil && err != errorNoSuchCommenter {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	/*
	if claims.Name != "" {
		name = claims.Name
	} else if claims.FamilyName != "" && claims.GivenName != "" {
		name = claims.GivenName + " " + claims.FamilyName
	} else if claims.DisplayName != "" {
		name = claims.DisplayName
	}
	*/

	link := "undefined"

	photo := "undefined"

	if claims.Picture != "" {
		photo = claims.Picture
	}

	var commenterHex string

	if err == errorNoSuchCommenter {
		commenterHex, err = commenterNew(email, "anonymous", link, photo, "huawei", "")
		if err != nil {
			bodyMarshal(w, response{"success": false, "message": err.Error()})
			return
		}
	} else {
		if err = commenterUpdate(c.CommenterHex, email, c.Name, link, photo, "huawei"); err != nil {
			logger.Warningf("cannot update commenter: %s", err)
			// not a serious enough to exit with an error
		}

		commenterHex = c.CommenterHex
	}

	if err := commenterSessionUpdate(commenterToken, commenterHex); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{"success": true})
}
