package main


import (
	"bytes"
	"fmt"
	ht "html/template"
	"net/smtp"
	"os"
	tt "text/template"
	"github.com/leonelquinteros/gotext"
	"mime"
	"strings"
)

type emailNotificationPlugs struct {
	Origin               string
	UnsubscribeSecretHex string
	Domain               string
	Path                 string
	CommentHex           string
	CommenterName        string
	Title                string
	Html                 ht.HTML
	Locale               *gotext.Locale
	Lang                 string
}

func smtpEmailNotification(to string, toName string, kind string, domain string, path string, commentHex string, commenterName string, title string, html string, unsubscribeSecretHex string, lang string) error {
	// checking email address
	splits := strings.Split(to, "@")
	if splits[1] == os.Getenv("HUAWEI_FAKE_EMAIL_DOMAIN") {
		return nil
	}

	// Configuring locale
	localesDir := fmt.Sprintf("%s/locales", os.Getenv("STATIC"))
	locale := gotext.NewLocale(localesDir, lang)
	locale.AddDomain("email-notifications")

	// headers
	h, err := tt.New("header").Parse(`MIME-Version: 1.0
From: {{.FromName}} <{{.FromAddress}}>
To: {{.ToName}} <{{.ToAddress}}>
Content-Type: text/html; charset=UTF-8
Subject: {{.Subject}}

`)
	var header bytes.Buffer
	fromName := locale.Get("YoWindow Weather")

	h.Execute(&header, &headerPlugs{
		FromAddress: os.Getenv("SMTP_FROM_ADDRESS"),
		FromName: mime.QEncoding.Encode("utf-8", fromName),
		ToAddress: to,
		ToName: toName,
		Subject: mime.QEncoding.Encode("utf-8", "[" + fromName + "] " + title),
	})

	// body
	templateFilename     := fmt.Sprintf("email-notification.%s.txt", kind)
	templateFullFilename := fmt.Sprintf("%s/templates/%s", os.Getenv("STATIC"), templateFilename)
	templateFunctions := ht.FuncMap{
		"rawHTML": func(s string)(ht.HTML) { return ht.HTML(s) },
		"printp":  func(s string, vars ...string)(string) { return printp( s, vars...) },
	}
	t, err := ht.New(templateFilename).Funcs( templateFunctions ).ParseFiles( templateFullFilename )


	if err != nil {
		logger.Errorf("cannot parse %s: %v", templateFullFilename, err)
		return errorMalformedTemplate
	}

	var body bytes.Buffer
	err = t.Execute(&body, &emailNotificationPlugs{
		Origin:               os.Getenv("ORIGIN"),
		Domain:               domain,
		Path:                 path,
		CommentHex:           commentHex,
		CommenterName:        commenterName,
		Title:                title,
		Html:                 ht.HTML(html),
		UnsubscribeSecretHex: unsubscribeSecretHex,
		Locale:               locale,
		Lang:                 lang,
	})
	if err != nil {
		logger.Errorf("error generating templated HTML for email notification: %v", err)
		return err
	}

	err = smtp.SendMail(os.Getenv("SMTP_HOST")+":"+os.Getenv("SMTP_PORT"), smtpAuth, os.Getenv("SMTP_FROM_ADDRESS"), []string{to}, concat(header, body))
	if err != nil {
		logger.Errorf("cannot send email notification: %v", err)
		return errorCannotSendEmail
	}

	return nil
}
