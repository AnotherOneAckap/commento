package main

import (
	"net/http"
)

// for given commentHex it goess up till root and returns root parent comment's commentHex and its position in root comments list, if sorted as "newest top"
func commentContextHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommentHex *string `json:"CommentHex"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	comment, err := commentGetByCommentHex(*x.CommentHex)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	// looking for root parent
	parentComment := comment
	parentCommentHex := "root"
	level := 0

	if comment.ParentHex != "root" {
		parentComment, err := commentGetByCommentHex(comment.ParentHex)
		if err != nil {
			bodyMarshal(w, response{"success": false, "message": err.Error()})
			return
		}

		for parentComment.ParentHex != "root" {
			parentComment, err = commentGetByCommentHex(parentComment.ParentHex)
			if err != nil {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}

			level = level + 1
			if level > 20 {
				logger.Errorf("deep recursion when looking for parent for comment %v", comment.CommentHex)
				bodyMarshal(w, response{"success": false, "message": "Server error"})
				return
			}
		}
		parentCommentHex = parentComment.CommentHex
	}

	// calculating position
	statement := `SELECT count(*) FROM ( SELECT commentHex FROM comments WHERE creationdate > $1 AND parentHex = 'root' AND path = $2 ORDER BY creationdate DESC ) c`
	row := db.QueryRow(statement, parentComment.CreationDate, parentComment.Path)

	var parentPosition int
	if err := row.Scan(&parentPosition); err != nil {
		logger.Errorf("cannot scan count: %v", err)
		bodyMarshal(w, response{"success": false, "message": "Server error"})
		return
	}

	bodyMarshal(w, response{
		"success":            true,
		"rootParentHex":      parentCommentHex,
		"rootParentPosition": parentPosition,
	})
}
