package main

import (
	"fmt"
	"database/sql"
	"net/http"
)

const COMMENTS_PER_PAGE = 10
const SORT_UPVOTES int = 0
const SORT_NEWEST  int = 1
const SORT_OLDEST  int = 2

func commentList2(commenterHex string, domain string, path string, includeUnapproved bool, parentHex string, offset int, limit int, sort int) ([]comment, map[string]commenter, int, error) {
	// path can be empty
	if commenterHex == "" || domain == "" {
		return nil, nil, 0, errorMissingField
	}

	fields := `
			c.commentHex,
			c.commenterHex,
			c.markdown,
			c.html,
			c.parentHex,
			c.score,
			c.state,
			c.deleted,
			c.creationDate,
			c.repliesCount
	`

	var statement string;

	if commenterHex == "anonymous" {
		statement = `
			SELECT %v
			FROM comments c
			JOIN commenters
				ON c.commenterHex = commenters.commenterHex
			WHERE
				c.deleted = false AND
				c.parentHex = $1 AND
				c.domain = $2 AND
				c.path = $3 AND $4 = $4 AND
				commenters.is_banned = false
		`
	} else {
		statement = `
			SELECT %v
			FROM comments c
			JOIN commenters
				ON c.commenterHex = commenters.commenterHex
			LEFT JOIN commenter_blocks cb
				ON cb.blockedCommenterHex = c.commenterHex AND cb.commenterHex = $4
			WHERE
				c.deleted = false AND
				c.parentHex = $1 AND
				c.domain = $2 AND
				c.path = $3 AND cb.blockedCommenterHex is null AND
				commenters.is_banned = false
		`
	}

	if !includeUnapproved {
		if commenterHex == "anonymous" {
			statement += `AND c.state = 'approved'`
		} else {
			statement += `AND (c.state = 'approved' OR c.commenterHex = $4)`
		}
	}

	countStatement := fmt.Sprintf( statement, "count(*)" )
	statement = fmt.Sprintf( statement, fields )

	var rows *sql.Rows
	var err error
	var total int

	rows, err = db.Query(countStatement, parentHex, domain, path, commenterHex)

	if err != nil {
		logger.Errorf("cannot get count: %v", err)
	}

	for rows.Next() {
		if err := rows.Scan(&total); err != nil {
			logger.Errorf("cannot scan count: %v", err)
		}
	}

	switch sort {
		case SORT_UPVOTES:
			statement += " ORDER BY score desc"
		case SORT_NEWEST:
			statement += " ORDER BY creationDate desc"
		case SORT_OLDEST:
			statement += " ORDER BY creationDate asc"
		default:
			statement += " ORDER BY score desc"
	}

	statement += ` LIMIT $5 OFFSET $6`
	rows, err = db.Query(statement, parentHex, domain, path, commenterHex, limit, offset)

	if err != nil {
		logger.Errorf("cannot get comments: %v", err)
		return nil, nil, 0, errorInternal
	}
	defer rows.Close()

	commenters := make(map[string]commenter)
	commenters["anonymous"] = commenter{CommenterHex: "anonymous", Email: "undefined", Name: "Anonymous", Link: "undefined", Photo: "undefined", Provider: "undefined"}

	comments := []comment{}
	for rows.Next() {
		c := comment{}
		if err = rows.Scan(
			&c.CommentHex,
			&c.CommenterHex,
			&c.Markdown,
			&c.Html,
			&c.ParentHex,
			&c.Score,
			&c.State,
			&c.Deleted,
			&c.CreationDate,
			&c.RepliesCount); err != nil {
			logger.Errorf("cannot parse database response: %v", err)
			return nil, nil, 0, errorInternal
		}

		if commenterHex != "anonymous" {
			statement = `
				SELECT direction
				FROM votes
				WHERE commentHex=$1 AND commenterHex=$2;
			`
			row := db.QueryRow(statement, c.CommentHex, commenterHex)

			if err = row.Scan(&c.Direction); err != nil {
				// TODO: is the only error here that there is no such entry?
				c.Direction = 0
			}
		}

		if commenterHex != c.CommenterHex {
			c.Markdown = ""
		}

		if !includeUnapproved {
			c.State = ""
		}

		comments = append(comments, c)

		if _, ok := commenters[c.CommenterHex]; !ok {
			commenters[c.CommenterHex], err = commenterGetByHex(c.CommenterHex)
			if err != nil {
				logger.Errorf("cannot retrieve commenter: %v", err)
				return nil, nil, 0, errorInternal
			}
		}
	}

	return comments, commenters, total, nil
}

func commentList2Handler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"CommenterToken"`
		ParentHex      *string `json:"parentHex"`
		Domain         *string `json:"domain"`
		Path           *string `json:"path"`
		Page           *int    `json:"page" commento_api:"optional"`
		Sort           *string `json:"sort" commento_api:"optional"`
		PerPage        *int    `json:"per_page" commento_api:"optional"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	domain := domainStrip(*x.Domain)
	path := *x.Path

	d, err := domainGet(domain)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	p, err := pageGet(domain, path)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	parentHex := *x.ParentHex

	commenterHex := "anonymous"
	isModerator := false
	modList := map[string]bool{}

	if *x.CommenterToken != "anonymous" {
		c, err := commenterGetByCommenterToken(*x.CommenterToken)
		if err != nil {
			if err == errorNoSuchToken {
				commenterHex = "anonymous"
			} else {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}
		} else {
			commenterHex = c.CommenterHex
		}

		for _, mod := range d.Moderators {
			modList[mod.Email] = true
			if mod.Email == c.Email {
				isModerator = true
			}
		}
	} else {
		for _, mod := range d.Moderators {
			modList[mod.Email] = true
		}
	}

	domainViewRecord(domain, commenterHex)

	// per_page
	var perPage int

	if x.PerPage == nil {
		perPage = COMMENTS_PER_PAGE
	} else {
		perPage = *x.PerPage
	}

	// offset
	var offset int

	if x.Page == nil {
		offset = 0
	} else {
		offset = *x.Page * perPage
	}

	// sort
	var sort int

	if x.Sort == nil {
		sort = SORT_UPVOTES
	} else {
		switch *x.Sort {
			case "upvotes":
				sort = SORT_UPVOTES
			case "oldest":
				sort = SORT_OLDEST
			case "newest":
				sort = SORT_NEWEST
			default:
				sort = SORT_UPVOTES
		}
	}

	comments, commenters, total, err := commentList2(commenterHex, domain, path, isModerator, parentHex, offset, perPage, sort)

	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	_commenters := map[string]commenter{}

	for commenterHex, cr := range commenters {
		if _, ok := modList[cr.Email]; ok {
			cr.IsModerator = true
		}
		cr.Email = ""
		_commenters[commenterHex] = cr
	}

	bodyMarshal(w, response{
		"success":               true,
		"domain":                domain,
		"totalComments":         total,
		"comments":              comments,
		"commenters":            _commenters,
		"requireModeration":     d.RequireModeration,
		"requireIdentification": d.RequireIdentification,
		"isFrozen":              d.State == "frozen",
		"isModerator":           isModerator,
		"defaultSortPolicy":     d.DefaultSortPolicy,
		"attributes":            p,
		"configuredOauths": map[string]bool{
			"commento": d.CommentoProvider,
			"google":   googleConfigured && d.GoogleProvider,
			"twitter":  twitterConfigured && d.TwitterProvider,
			"github":   githubConfigured && d.GithubProvider,
			"gitlab":   gitlabConfigured && d.GitlabProvider,
			"sso":      d.SsoProvider,
		},
	})
}
