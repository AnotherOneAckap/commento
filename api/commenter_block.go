package main

import (
	"net/http"
)

func commenterBlock( commenter *commenter, blockedCommenter *commenter, reasonCommentHex *string ) error {
	statement := `
		INSERT INTO commenter_blocks (commenterHex, blockedCommenterHex, reasonCommentHex)
		VALUES ($1, $2, $3);
	`
	_, err := db.Exec(statement, commenter.CommenterHex, blockedCommenter.CommenterHex, *reasonCommentHex)
	if err != nil {
		logger.Errorf("cannot update commenter_blocks: %v", err)
		return errorInternal
	}

	return nil
}

func commenterBlockHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
		CommenterHex   *string `json:"commenterHex"`
		CommentHex     *string `json:"commentHex"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c1, err := commenterGetByHex(*x.CommenterHex)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	err = commenterBlock(&c, &c1, x.CommentHex);
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{"success": true})
}
