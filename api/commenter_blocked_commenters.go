package main

import (
	"net/http"
)

func commenterBlockedCommenters( currentCommenter *commenter ) ([]commenter, error) {
	if currentCommenter.CommenterHex == "anonymous" {
		return []commenter{}, nil;
	}

	statement := `
		SELECT DISTINCT
			c.CommenterHex,
			c.Name,
			c.Link,
			c.Photo
		FROM commenter_blocks cb
		JOIN commenters c
			ON cb.blockedCommenterHex = c.commenterHex AND cb.commenterHex = $1
	`
	rows, err := db.Query(statement, currentCommenter.CommenterHex)
	if err != nil {
		logger.Errorf("cannot update commenter_blocks: %v", err)
		return nil, errorInternal
	}

	blockedCommenters := []commenter{}

	for rows.Next() {
		c := commenter{}
		if err = rows.Scan(
			&c.CommenterHex,
			&c.Name,
			&c.Link,
			&c.Photo,
			); err != nil {
			logger.Errorf("cannot parse database response: %v", err)
			return nil, errorInternal
		}

		blockedCommenters = append(blockedCommenters, c)

	}

	return blockedCommenters, nil
}

func commenterBlockedCommentersHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	blockedCommenters, err := commenterBlockedCommenters(&c)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{
		"success":    true,
		"blockedCommenters": blockedCommenters,
	})
}
