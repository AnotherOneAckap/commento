package main

import (
	"net/http"
)

func commenterBanned() ([]commenter, error) {
	statement := `
		SELECT DISTINCT
			c.CommenterHex,
			c.Name,
			c.Link,
			c.Photo
		FROM commenters c
		WHERE c.is_banned = true
	`
	rows, err := db.Query(statement)
	if err != nil {
		logger.Errorf("cannot select commenters: %v", err)
		return nil, errorInternal
	}

	bannedCommenters := []commenter{}

	for rows.Next() {
		c := commenter{}
		if err = rows.Scan(
			&c.CommenterHex,
			&c.Name,
			&c.Link,
			&c.Photo,
			); err != nil {
			logger.Errorf("cannot parse database response: %v", err)
			return nil, errorInternal
		}

		bannedCommenters = append(bannedCommenters, c)

	}

	return bannedCommenters, nil
}

func commenterBannedHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
		Domain         *string `json:"domain"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	domain := domainStrip(*x.Domain)
	d, err := domainGet(domain)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	isModerator := false

	for _, mod := range d.Moderators {
		if mod.Email == c.Email {
			isModerator = true
			break
		}
	}

	if ! isModerator {
		bodyMarshal(w, response{"success": false, "message": "Access denied"})
		return
	}

	bannedCommenters, err := commenterBanned()
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{
		"success":    true,
		"bannedCommenters": bannedCommenters,
	})
}
