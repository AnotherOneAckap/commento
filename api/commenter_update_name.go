package main

import (
	"net/http"
)

func commenterUpdateName(commenterHex string, provider string, name string) error {
	if name == "" {
		return errorMissingField
	}

	statement := `
		UPDATE commenters
		SET name = $3
		WHERE commenterHex = $1 and provider = $2;
	`
	_, err := db.Exec(statement, commenterHex, provider, name)
	if err != nil {
		logger.Errorf("cannot update commenter: %v", err)
		return errorInternal
	}

	return nil
}

func commenterUpdateNameHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
		Name           *string `json:"name"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	if c.Provider != "huawei" {
		bodyMarshal(w, response{"success": false, "message": errorCannotUpdateOauthProfile.Error()})
		return
	}

	if err = commenterUpdateName(c.CommenterHex, c.Provider, *x.Name); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{"success": true})
}
