package main

import (
	"database/sql"
	"net/http"
	"github.com/lib/pq"
)

func commentSubtreeList(commenterHex string, domain string, path string, includeUnapproved bool, parentHexs []string) ([]comment, map[string]commenter, error) {
	// path can be empty
	if commenterHex == "" || domain == "" {
		return nil, nil, errorMissingField
	}

	var statement string;

	if commenterHex == "anonymous" {
		statement = `
			SELECT
				comments.commentHex,
				comments.commenterHex,
				comments.markdown,
				comments.html,
				comments.parentHex,
				comments.score,
				comments.state,
				comments.deleted,
				comments.creationDate,
				comments.repliesCount
			FROM comments
			JOIN commenters
				ON comments.commenterHex = commenters.commenterHex
			WHERE
				comments.deleted = false AND
				comments.parentHex = ANY($1) AND
				comments.domain = $2 AND
				comments.path = $3 AND
				commenters.is_banned = false
		`
	} else {
		statement = `
			SELECT
				comments.commentHex,
				comments.commenterHex,
				comments.markdown,
				comments.html,
				comments.parentHex,
				comments.score,
				comments.state,
				comments.deleted,
				comments.creationDate,
				comments.repliesCount
			FROM comments
			JOIN commenters
				ON comments.commenterHex = commenters.commenterHex
			LEFT JOIN commenter_blocks cb
				ON cb.blockedCommenterHex = comments.commenterHex AND cb.commenterHex = $4
			WHERE
				comments.deleted = false AND
				comments.parentHex = ANY($1) AND
				comments.domain = $2 AND
				comments.path = $3 AND
				commenters.is_banned = false
		`
	}

	if commenterHex == "anonymous" {
		statement += `AND comments.state = 'approved'`
	} else {
		if includeUnapproved {
			statement += `AND comments.commenterHex = $4`
		} else {
			statement += `AND (comments.state = 'approved' OR comments.commenterHex = $4)`
		}
	}

	var rows *sql.Rows
	var err error

	if commenterHex == "anonymous" {
		rows, err = db.Query(statement, pq.Array(parentHexs), domain, path)
	} else {
		rows, err = db.Query(statement, pq.Array(parentHexs), domain, path, commenterHex)
	}

	if err != nil {
		logger.Errorf("cannot get comments: %v", err)
		return nil, nil, errorInternal
	}
	defer rows.Close()

	commenters := make(map[string]commenter)
	commenters["anonymous"] = commenter{CommenterHex: "anonymous", Email: "undefined", Name: "Anonymous", Link: "undefined", Photo: "undefined", Provider: "undefined"}

	comments := []comment{}
	for rows.Next() {
		c := comment{}
		if err = rows.Scan(
			&c.CommentHex,
			&c.CommenterHex,
			&c.Markdown,
			&c.Html,
			&c.ParentHex,
			&c.Score,
			&c.State,
			&c.Deleted,
			&c.CreationDate,
			&c.RepliesCount); err != nil {
			logger.Errorf("cannot parse database response: %v", err)
			return nil, nil, errorInternal
		}

		if commenterHex != "anonymous" {
			statement = `
				SELECT direction
				FROM votes
				WHERE commentHex=$1 AND commenterHex=$2;
			`
			row := db.QueryRow(statement, c.CommentHex, commenterHex)

			if err = row.Scan(&c.Direction); err != nil {
				// TODO: is the only error here that there is no such entry?
				c.Direction = 0
			}
		}

		if commenterHex != c.CommenterHex {
			c.Markdown = ""
		}

		if !includeUnapproved {
			c.State = ""
		}

		comments = append(comments, c)

		if _, ok := commenters[c.CommenterHex]; !ok {
			commenters[c.CommenterHex], err = commenterGetByHex(c.CommenterHex)
			if err != nil {
				logger.Errorf("cannot retrieve commenter: %v", err)
				return nil, nil, errorInternal
			}
		}
	}

	return comments, commenters, nil
}

func commentSubtreeListHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"CommenterToken"`
		Domain         *string `json:"domain"`
		Path           *string `json:"path"`
		ParentHex      *string `json:"parentHex"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	domain := domainStrip(*x.Domain)
	path := *x.Path

	d, err := domainGet(domain)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	p, err := pageGet(domain, path)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	parentHexs := []string{"root"}
	commenterHex := "anonymous"
	isModerator := false
	modList := map[string]bool{}

	if *x.CommenterToken != "anonymous" {
		c, err := commenterGetByCommenterToken(*x.CommenterToken)
		if err != nil {
			if err == errorNoSuchToken {
				commenterHex = "anonymous"
			} else {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}
		} else {
			commenterHex = c.CommenterHex
		}

		for _, mod := range d.Moderators {
			modList[mod.Email] = true
			if mod.Email == c.Email {
				isModerator = true
			}
		}
	} else {
		for _, mod := range d.Moderators {
			modList[mod.Email] = true
		}
	}

	if x.ParentHex != nil {
		parentHexs = []string{ *x.ParentHex }
	}

	domainViewRecord(domain, commenterHex)

	// others
	comments, commenters, err := commentSubtreeList(commenterHex, domain, path, isModerator, parentHexs)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	_commenters := map[string]commenter{}

	for commenterHex, cr := range commenters {
		if _, ok := modList[cr.Email]; ok {
			cr.IsModerator = true
		}
		cr.Email = ""
		_commenters[commenterHex] = cr
	}

	currentLevelComments := comments
	hasMoreComments := true

	// loading children comments recursively, 50th level is emergency exit
	for level := 0; hasMoreComments; level++ {
		if level >= 50 {
			panic("recursion too deep")
		}

		parentHexs = make([]string, 0)

		for _, cr := range currentLevelComments {
			if cr.RepliesCount > 0 {
				parentHexs = append(parentHexs, cr.CommentHex);
			}
		}

		if len(parentHexs) > 0 {
			moreComments, moreCommenters, err := commentMoreList(commenterHex, domain, path, isModerator, parentHexs)

			if err != nil {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}

			currentLevelComments = moreComments
			comments = append( comments, moreComments... )

			// now add moreCommenters
			for commenterHex, cr := range moreCommenters {
				if _, ok := modList[cr.Email]; ok {
					cr.IsModerator = true
				}
				cr.Email = ""
				_commenters[commenterHex] = cr
			}
		} else {
			hasMoreComments = false
		}
	}

	// adding comment itself
	c, err := commentGetByCommentHex( *x.ParentHex )
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	comments = append(comments, c)

	if _, ok := _commenters[c.CommenterHex]; !ok {
		_commenters[c.CommenterHex], err = commenterGetByHex(c.CommenterHex)
		if err != nil {
			logger.Errorf("cannot retrieve commenter: %v", err)
			bodyMarshal(w, response{"success": false, "message": err.Error()})
			return
		}
	}

	bodyMarshal(w, response{
		"success":               true,
		"domain":                domain,
		"comments":              comments,
		"commenters":            _commenters,
		"requireModeration":     d.RequireModeration,
		"requireIdentification": d.RequireIdentification,
		"isFrozen":              d.State == "frozen",
		"isModerator":           isModerator,
		"defaultSortPolicy":     d.DefaultSortPolicy,
		"attributes":            p,
		"configuredOauths": map[string]bool{
			"commento": d.CommentoProvider,
			"google":   googleConfigured && d.GoogleProvider,
			"twitter":  twitterConfigured && d.TwitterProvider,
			"github":   githubConfigured && d.GithubProvider,
			"gitlab":   gitlabConfigured && d.GitlabProvider,
			"sso":      d.SsoProvider,
		},
	})
}

