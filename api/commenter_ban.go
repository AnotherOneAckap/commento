package main

import (
	"net/http"
)

func commenterBan( commenter *commenter ) error {
	statement := `
		UPDATE commenters SET is_banned = true WHERE commenterHex = $1
	`
	_, err := db.Exec(statement, commenter.CommenterHex)
	if err != nil {
		logger.Errorf("cannot update commenters: %v", err)
		return errorInternal
	}

	return nil
}

func commenterBanHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
		CommenterHex   *string `json:"commenterHex"`
		Domain         *string `json:"domain"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	domain := domainStrip(*x.Domain)
	d, err := domainGet(domain)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	isModerator := false

	for _, mod := range d.Moderators {
		if mod.Email == c.Email {
			isModerator = true
			break
		}
	}

	if ! isModerator {
		bodyMarshal(w, response{"success": false, "message": "Access denied"})
		return
	}

	c1, err := commenterGetByHex(*x.CommenterHex)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	err = commenterBan(&c1);
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{"success": true})
}
