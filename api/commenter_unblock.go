package main

import (
	"net/http"
)

func commenterUnblock( commenter *commenter, blockedCommenter *commenter ) error {
	statement := `
		DELETE FROM commenter_blocks WHERE commenterHex = $1 AND blockedCommenterHex = $2
	`
	_, err := db.Exec(statement, commenter.CommenterHex, blockedCommenter.CommenterHex)
	if err != nil {
		logger.Errorf("cannot update commenter_blocks: %v", err)
		return errorInternal
	}

	return nil
}

func commenterUnblockHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"commenterToken"`
		CommenterHex   *string `json:"commenterHex"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c, err := commenterGetByCommenterToken(*x.CommenterToken)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	c1, err := commenterGetByHex(*x.CommenterHex)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	err = commenterUnblock(&c, &c1);
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	bodyMarshal(w, response{"success": true})
}
