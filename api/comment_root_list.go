package main

import (
	"database/sql"
	"net/http"
	"github.com/lib/pq"
)

func commentMoreList(commenterHex string, domain string, path string, includeUnapproved bool, parentHexs []string) ([]comment, map[string]commenter, error) {
	// path can be empty
	if commenterHex == "" || domain == "" {
		return nil, nil, errorMissingField
	}

	statement := `
		SELECT
			c.commentHex,
			c.commenterHex,
			c.markdown,
			c.html,
			c.parentHex,
			c.score,
			c.state,
			c.deleted,
			c.creationDate,
			c.repliesCount
		FROM comments c
		JOIN commenters
			ON c.commenterHex = commenters.commenterHex
		WHERE
			c.deleted = false AND
			c.parentHex = ANY($1) AND
			c.domain = $2 AND
			c.path = $3 AND
			commenters.is_banned = false
	`

	if !includeUnapproved {
		if commenterHex == "anonymous" {
			statement += `AND c.state = 'approved'`
		} else {
			statement += `AND (c.state = 'approved' OR c.commenterHex = $4)`
		}
	}

	var rows *sql.Rows
	var err error

	if !includeUnapproved && commenterHex != "anonymous" {
		rows, err = db.Query(statement, pq.Array(parentHexs), domain, path, commenterHex)
	} else {
		rows, err = db.Query(statement, pq.Array(parentHexs), domain, path)
	}

	if err != nil {
		logger.Errorf("cannot get comments: %v", err)
		return nil, nil, errorInternal
	}
	defer rows.Close()

	commenters := make(map[string]commenter)
	commenters["anonymous"] = commenter{CommenterHex: "anonymous", Email: "undefined", Name: "Anonymous", Link: "undefined", Photo: "undefined", Provider: "undefined"}

	comments := []comment{}
	for rows.Next() {
		c := comment{}
		if err = rows.Scan(
			&c.CommentHex,
			&c.CommenterHex,
			&c.Markdown,
			&c.Html,
			&c.ParentHex,
			&c.Score,
			&c.State,
			&c.Deleted,
			&c.CreationDate,
			&c.RepliesCount); err != nil {
			logger.Errorf("cannot parse database response: %v", err)
			return nil, nil, errorInternal
		}

		if commenterHex != "anonymous" {
			statement = `
				SELECT direction
				FROM votes
				WHERE commentHex=$1 AND commenterHex=$2;
			`
			row := db.QueryRow(statement, c.CommentHex, commenterHex)

			if err = row.Scan(&c.Direction); err != nil {
				// TODO: is the only error here that there is no such entry?
				c.Direction = 0
			}
		}

		if commenterHex != c.CommenterHex {
			c.Markdown = ""
		}

		if !includeUnapproved {
			c.State = ""
		}

		comments = append(comments, c)

		if _, ok := commenters[c.CommenterHex]; !ok {
			commenters[c.CommenterHex], err = commenterGetByHex(c.CommenterHex)
			if err != nil {
				logger.Errorf("cannot retrieve commenter: %v", err)
				return nil, nil, errorInternal
			}
		}
	}

	return comments, commenters, nil
}

func commentRootListHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		CommenterToken *string `json:"CommenterToken"`
		Domain         *string `json:"domain"`
		Path           *string `json:"path"`
		Page           *int    `json:"page"`
		Sort           *string `json:"sort"`
		PerPage        *int    `json:"per_page"`
	}

	var x request
	if err := bodyUnmarshal(r, &x); err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	domain := domainStrip(*x.Domain)
	path := *x.Path

	d, err := domainGet(domain)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	p, err := pageGet(domain, path)
	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	parentHex := "root"
	commenterHex := "anonymous"
	isModerator := false
	modList := map[string]bool{}

	if *x.CommenterToken != "anonymous" {
		c, err := commenterGetByCommenterToken(*x.CommenterToken)
		if err != nil {
			if err == errorNoSuchToken {
				commenterHex = "anonymous"
			} else {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}
		} else {
			commenterHex = c.CommenterHex
		}

		for _, mod := range d.Moderators {
			modList[mod.Email] = true
			if mod.Email == c.Email {
				isModerator = true
			}
		}
	} else {
		for _, mod := range d.Moderators {
			modList[mod.Email] = true
		}
	}

	domainViewRecord(domain, commenterHex)

	// per_page
	var perPage int

	if x.PerPage == nil {
		perPage = 10
	} else {
		perPage = *x.PerPage
	}

	// offset
	var offset int

	if x.Page == nil {
		offset = 0
	} else {
		offset = ( *x.Page - 1 ) * perPage
	}

	// sort
	var sort int

	if x.Sort == nil {
		sort = SORT_UPVOTES
	} else {
		switch *x.Sort {
			case "upvotes":
				sort = SORT_UPVOTES
			case "oldest":
				sort = SORT_OLDEST
			case "newest":
				sort = SORT_NEWEST
			default:
				sort = SORT_UPVOTES
		}
	}

	comments, commenters, total, err := commentList2(commenterHex, domain, path, isModerator, parentHex, offset, perPage, sort)

	if err != nil {
		bodyMarshal(w, response{"success": false, "message": err.Error()})
		return
	}

	_commenters := map[string]commenter{}

	for commenterHex, cr := range commenters {
		if _, ok := modList[cr.Email]; ok {
			cr.IsModerator = true
		}
		cr.Email = ""
		_commenters[commenterHex] = cr
	}

	currentLevelComments := comments
	hasMoreComments := true

	// loading children comments till 3rd level
	for level := 0; level < 2 && hasMoreComments; level++ {
		var parentHexs []string

		for _, cr := range currentLevelComments {
			if cr.RepliesCount > 0 {
				parentHexs = append(parentHexs, cr.CommentHex);
			}
		}

		if len(parentHexs) > 0 {
			moreComments, moreCommenters, err := commentMoreList(commenterHex, domain, path, isModerator, parentHexs)

			if err != nil {
				bodyMarshal(w, response{"success": false, "message": err.Error()})
				return
			}

			currentLevelComments = moreComments
			comments = append( comments, moreComments... )

			// now add moreCommenters
			for commenterHex, cr := range moreCommenters {
				if _, ok := modList[cr.Email]; ok {
					cr.IsModerator = true
				}
				cr.Email = ""
				_commenters[commenterHex] = cr
			}
		} else {
			hasMoreComments = false
		}
	}

	bodyMarshal(w, response{
		"success":               true,
		"domain":                domain,
		"totalComments":         total,
		"comments":              comments,
		"commenters":            _commenters,
		"requireModeration":     d.RequireModeration,
		"requireIdentification": d.RequireIdentification,
		"isFrozen":              d.State == "frozen",
		"isModerator":           isModerator,
		"defaultSortPolicy":     d.DefaultSortPolicy,
		"attributes":            p,
		"configuredOauths": map[string]bool{
			"commento": d.CommentoProvider,
			"google":   googleConfigured && d.GoogleProvider,
			"twitter":  twitterConfigured && d.TwitterProvider,
			"github":   githubConfigured && d.GithubProvider,
			"gitlab":   gitlabConfigured && d.GitlabProvider,
			"sso":      d.SsoProvider,
		},
	})
}

