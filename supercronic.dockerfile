FROM golang:1.20-alpine

ARG COMMENTO_BACKUP_SSH_KEY

RUN apk add git openssh postgresql12-client

RUN go install github.com/aptible/supercronic@v0.2.25

WORKDIR /app

COPY crontab /app

COPY scripts/make_commento_backup.sh /app/scripts/

RUN echo db:5432:commento:postgres:postgres > /root/.pgpass && chmod 0600 /root/.pgpass

RUN mkdir /root/.ssh && echo 'u325463.your-storagebox.de ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA5EB5p/5Hp3hGW1oHok+PIOH9Pbn7cnUiGmUEBrCVjnAw+HrKyN8bYVV0dIGllswYXwkG/+bgiBlE6IVIBAq+JwVWu1Sss3KarHY3OvFJUXZoZyRRg/Gc/+LRCE7lyKpwWQ70dbelGRyyJFH36eNv6ySXoUYtGkwlU5IVaHPApOxe4LHPZa/qhSRbPo2hwoh0orCtgejRebNtW5nlx00DNFgsvn8Svz2cIYLxsPVzKgUxs8Zxsxgn+Q/UvR7uq4AbAhyBMLxv7DjJ1pc7PJocuTno2Rw9uMZi1gkjbnmiOh6TTXIEWbnroyIhwc8555uto9melEUmWNQ+C+PwAK+MPw==' > /root/.ssh/known_hosts

ENTRYPOINT [ "supercronic", "/app/crontab" ]
