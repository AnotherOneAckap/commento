ALTER TABLE comments
  ADD repliesCount INTEGER NOT NULL DEFAULT 0;

CREATE OR REPLACE FUNCTION commentsInsertTriggerFunction2() RETURNS TRIGGER AS $trigger$
BEGIN
  UPDATE comments
  SET repliesCount = repliesCount + 1
  WHERE commentHex = new.parentHex;

  RETURN NEW;
END;
$trigger$ LANGUAGE plpgsql;

CREATE TRIGGER commentsInsertTrigger2 AFTER INSERT ON comments
FOR EACH ROW EXECUTE PROCEDURE commentsInsertTriggerFunction2();

UPDATE comments c1 SET repliesCount = (SELECT COUNT(*) FROM comments WHERE parentHex = c1.commentHex );
