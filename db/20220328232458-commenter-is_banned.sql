ALTER TABLE commenters
  ADD is_banned boolean not null default false;

CREATE INDEX commentersIsBanned ON commenters(is_banned);
