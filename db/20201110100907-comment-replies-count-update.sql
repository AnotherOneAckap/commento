CREATE OR REPLACE FUNCTION commentsUpdateTriggerFunction() RETURNS TRIGGER AS $trigger$
BEGIN
	IF NEW.deleted THEN
		UPDATE comments SET repliesCount = repliesCount - 1 WHERE commentHex = NEW.parentHex;
	END IF;

	RETURN NEW;
END;
$trigger$ LANGUAGE 'plpgsql';

CREATE TRIGGER commentsUpdateTrigger AFTER UPDATE ON comments
FOR EACH ROW EXECUTE PROCEDURE commentsUpdateTriggerFunction();

UPDATE comments c1 SET repliesCount = ( SELECT COUNT(*) FROM comments WHERE deleted = false AND parentHex = c1.commentHex );
