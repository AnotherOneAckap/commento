CREATE TABLE IF NOT EXISTS commenter_blocks (
  commenterHex             TEXT          NOT NULL,
  blockedCommenterHex      TEXT          NOT NULL,
  reasonCommentHex         TEXT          NOT NULL
);
